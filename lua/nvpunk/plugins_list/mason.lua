return {
    -- plugin installer and manager for LSP, DAP, formatters and more
    {'williamboman/mason.nvim'},

    -- bridge mason and lspconfig
    {'williamboman/mason-lspconfig.nvim'},
}
